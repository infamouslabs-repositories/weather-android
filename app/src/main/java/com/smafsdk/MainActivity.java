package com.smafsdk;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.VideoView;
import com.smafsdk.common.AbstractActivity;
import com.smafsdk.tools.Constants;
import com.smafsdk.tools.JsSmafInterface;
import com.smafsdk.tools.LocalStorageInterface;


public class MainActivity extends AbstractActivity {

    /**
     * The current WebView
     */
    private WebView mWebView;

    /**
     * The current web settings of a webview
     */
    private WebSettings mWebSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = (WebView) findViewById(R.id.webviewSmaf);
        mWebSettings = mWebView.getSettings();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        // Init web view with necessary web settings
        _initWebView();
        _refreshWebView(Constants.URL_INDEX_HTML);
    }

    /**
     * Disable back button
     */
    @Override
    public void onBackPressed()
    {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {//check for browser web page history
            simulateKeyEvent("BACK", keyCode, true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {//check for browser web page history
            simulateKeyEvent("BACK", keyCode, false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Loads the URL you want in the webview
     */
    private void _refreshWebView(String url)
    {
        mWebView.loadUrl(url);
    }

    /**
     * Inits all the necessary web settings for webiew
     * Here you can enable JS, DomStorage etc..
     */
    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface", "AddJavascriptInterface"})
    private void _initWebView()
    {
        if (mWebSettings != null) {

            mWebSettings.setJavaScriptEnabled(true);
            mWebSettings.setDomStorageEnabled(true);
            mWebSettings.setDatabaseEnabled(true);

            JsSmafInterface smafInterface = new JsSmafInterface(this);
            LocalStorageInterface storageInterface = new LocalStorageInterface(this);

            mWebView.addJavascriptInterface(smafInterface, "SmafInterface");
            mWebView.addJavascriptInterface(storageInterface, "LocalStorageInterface");

            mWebView.setWebViewClient(new WebViewClient());
            mWebView.setWebChromeClient(new WebChromeClient());
            mWebView.setFocusableInTouchMode(false);
            mWebView.setInitialScale(getInitialScale());
        }
    }

    /**
     * Simulates the keyDown/keyup event
     * @param key
     * @param code
     * @param keyDown
     */
    private void simulateKeyEvent(String key, int code, boolean keyDown) {
        mWebView.loadUrl("javascript:(function(){var e=document.createEvent('KeyboardEvent');e.initKeyboardEvent('" + (keyDown ? "keydown" : "keyup") + "',true,true,null,'" + key + "',0,0,0," + code + ",0);document.body.dispatchEvent(e);})()");
    }
}
