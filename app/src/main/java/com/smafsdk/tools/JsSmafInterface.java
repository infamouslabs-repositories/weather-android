package com.smafsdk.tools;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.webkit.JavascriptInterface;

import com.google.gson.Gson;
import com.smafsdk.model.DeviceInfo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * Created by Smaf.tv on 6/9/15.
 */
public class JsSmafInterface
{
    /**
     * The application context into the activity which will
     * create the interface
     */
    private Context mContext;

    /**
     * A class with helpers
     */
    private Utilities utilities;

    /**
     * A JSON tool to convert object  to json and the opposite
     */
    private Gson mGson;

    public JsSmafInterface(Context context)
    {
        this.mContext = context;
        this.mGson = new Gson();
        utilities = Utilities.getInstance(mContext);
    }

    /**
     * Returns all the Device info
     * @return DeviceInfo object
     */
    @JavascriptInterface
    public String getDeviceInfo()
    {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setSdk(Build.VERSION.SDK_INT);
        deviceInfo.setDevice(Build.DEVICE);
        deviceInfo.setModel(Build.MODEL);
        deviceInfo.setProduct(Build.PRODUCT);
        deviceInfo.setManufacturer(Build.MANUFACTURER);
        Locale locale = this.mContext.getResources().getConfiguration().locale;
        deviceInfo.setCountry(locale.getCountry());
        deviceInfo.setLanguage(locale.getLanguage());
        deviceInfo.setLocale(locale.toString());
        return mGson.toJson(deviceInfo);
    }

    /**
     * Get a uniqueId per device
     * @return String
     */
    @JavascriptInterface
    public String getUniqueID()
    {
        //get device ID as IMEI
        String devIDShort = utilities.getPseudoIMEI();
        //gets android ID
        String androidID = utilities.getAndroidID();
        //gets MAC address
        String wlanMAC = utilities.getMacAddress();
        //combine all of them
        String longID = wlanMAC + devIDShort + androidID;

        //DIGEST to uniqueID
        String uniqueID = "";
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if (m != null) {
            m.update(longID.getBytes(), 0, longID.length());
            byte p_md5Data[] = m.digest();
            for (byte aP_md5Data : p_md5Data) {
                int b = (0xFF & aP_md5Data);
                // if it is a single digit, make sure it have 0 in front (proper
                // padding)
                if (b <= 0xF)
                    uniqueID += "0";
                // add number to string
                uniqueID += Integer.toHexString(b);
            }

        }
        return uniqueID;
    }
}
