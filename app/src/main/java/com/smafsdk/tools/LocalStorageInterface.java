package com.smafsdk.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.webkit.JavascriptInterface;

import com.smafsdk.db.LocalStorage;

/**
 * Created by Smaf.tv on 6/10/15.
 */
public class LocalStorageInterface
{
    private Context mContext;
    private LocalStorage localStorageDBHelper;
    private SQLiteDatabase database;

    public LocalStorageInterface(Context c) {
        mContext = c;
        localStorageDBHelper = LocalStorage.getInstance(mContext);
    }

    /**
     * Returns back the item based on the requested key
     * @param key : requested key to get back the item
     * @return the item having the given key
     */
    @JavascriptInterface
    public String getItem(String key)
    {
        String value = null;
        if(key != null) {
            database = localStorageDBHelper.getReadableDatabase();
            Cursor cursor = database.query(LocalStorage.LOCALSTORAGE_TABLE_NAME,
                    null,
                    LocalStorage.LOCALSTORAGE_ID + " = ?",
                    new String [] {key},null, null, null);

            if(cursor.moveToFirst()) {
                value = cursor.getString(1);
            }
            cursor.close();
            database.close();
        }
        return value;
    }

    /**
     * Stores key value pair
     * @param key
     * @param value
     */
    @JavascriptInterface
    public void setItem(String key,String value)
    {
        if(key != null && value != null) {
            String oldValue = getItem(key);
            database = localStorageDBHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(LocalStorage.LOCALSTORAGE_ID, key);
            values.put(LocalStorage.LOCALSTORAGE_VALUE, value);

            if(oldValue != null) {
                database.update(LocalStorage.LOCALSTORAGE_TABLE_NAME, values, LocalStorage.LOCALSTORAGE_ID + "='" + key + "'", null);
            } else {
                database.insert(LocalStorage.LOCALSTORAGE_TABLE_NAME, null, values);
            }
            database.close();
        }
    }

    /**
     * removes the item corresponding to the requested key
     * @param key
     */
    @JavascriptInterface
    public void removeItem(String key)
    {
        if(key != null) {
            database = localStorageDBHelper.getWritableDatabase();
            database.delete(LocalStorage.LOCALSTORAGE_TABLE_NAME, LocalStorage.LOCALSTORAGE_ID + "='" + key + "'", null);
            database.close();
        }
    }

    /**
     * clears the local storage.
     */
    @JavascriptInterface
    public void clear()
    {
        database = localStorageDBHelper.getWritableDatabase();
        database.delete(LocalStorage.LOCALSTORAGE_TABLE_NAME, null, null);
        database.close();
    }
}
