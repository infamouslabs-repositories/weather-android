package com.smafsdk.tools;


import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;

/**
 * Created by Smaf.tv on 6/9/15.
 */
public class Utilities {

    /**
     * The instance of the class for Singleton
     */
    private static Utilities _instance;

    /**
     * The application's context
     */
    private Context mContext;


    /**
     * Singleton patter
     *
     * @return
     */
    public static synchronized Utilities getInstance(Context context) {
        if (_instance == null)
            _instance = new Utilities(context);
        return _instance;
    }

    private Utilities(Context context) {
        this.mContext = context;
    }

    /**
     * Returns A 64-bit number (as a hex string) that is randomly generated when the user first
     * sets up the device and should remain constant for the lifetime of the user's device.
     * The value may change if a factory reset is performed on the device.
     *
     * @return
     */
    public String getAndroidID() {
        try {
            return Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the mac address of the device
     * @return String
     */
    public String getMacAddress()
    {
        try {
            WifiManager wm = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            return wm.getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns a HASH String formatted as an IMEI
     * @return String
     */
    public String getPseudoIMEI()
    {
        try {
            return "35" + // we make this look like a valid IMEI
                    Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +
                    Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +
                    Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +
                    Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +
                    Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +
                    Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +
                    Build.USER.length() % 10;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}